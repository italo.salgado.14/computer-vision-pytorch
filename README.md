# Repo with Pytorch code in many contexts - 2021

Códigos en pytorch de todos los tópicos generales de computer vision. Basado en Libro.

> Modern Computer Vision with PyTorch: Explore deep learning concepts and implement over 50 real-world image applications - V Kishore Ayyadevara, Yeshwanth Reddy. 

## Description

Codes for many purposes related with pytorch and computer vision.
I recommend using only one virtualenv for all projects.

File numbering: the folders contain the chapter numbers, while the "jupyter" name contains the ordered titles of the guide book.

These also contain personal comments.


## Sources

- [ ] [Source code](https://github.com/PacktPublishing/Modern-Computer-Vision-with-PyTorch).
- [ ] Modern Computer Vision with Pytorch. 

## Author

Italo Salgado, UTFSM, Ing. Civil Electrónico. 2021.
